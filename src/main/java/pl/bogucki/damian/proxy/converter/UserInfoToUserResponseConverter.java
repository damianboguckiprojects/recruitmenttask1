package pl.bogucki.damian.proxy.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.bogucki.damian.proxy.controller.model.UserResponse;
import pl.bogucki.damian.proxy.service.userinfo.types.UserInfo;

public class UserInfoToUserResponseConverter {
    private final static Logger logger = LoggerFactory.getLogger(UserInfoToUserResponseConverter.class);

    public static UserResponse convert(UserInfo userInfo) {
        logger.info("Converting " + UserInfo.class.getCanonicalName() + " to " + UserResponse.class.getCanonicalName());
        UserResponse userResponse = new UserResponse();
        userResponse.setId(userInfo.getId());
        userResponse.setLogin(userInfo.getLogin());
        userResponse.setName(userInfo.getName());
        userResponse.setType(userInfo.getType());
        userResponse.setAvatarUrl(userInfo.getAvatarUrl());
        userResponse.setCreatedAt(userInfo.getCreatedAt());
        userResponse.setCalculations(userInfo.getCalculations());
        return userResponse;
    }
}
