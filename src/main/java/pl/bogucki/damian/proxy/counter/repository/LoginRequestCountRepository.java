package pl.bogucki.damian.proxy.counter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pl.bogucki.damian.proxy.counter.model.LoginRequestCount;

public interface LoginRequestCountRepository extends JpaRepository<LoginRequestCount, Long> {

    @Transactional
    @Modifying
    @Query(value = "UPDATE #{#entityName} " +
            "SET requestCount = requestCount + 1 " +
            "WHERE login = :login")
    int increaseCounter(@Param("login") String login);
}
