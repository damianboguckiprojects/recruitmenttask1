package pl.bogucki.damian.proxy.counter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import pl.bogucki.damian.proxy.counter.model.LoginRequestCount;
import pl.bogucki.damian.proxy.counter.repository.LoginRequestCountRepository;

@Component
public class UserInfoRequestCounterServiceImpl implements UserInfoRequestCounterService{
    private LoginRequestCountRepository loginRequestCountRepository;

    @Override
    public void incrementCounter(String login) {
        if (loginRequestCountRepository.increaseCounter(login) == 0) {
            try {
                LoginRequestCount loginRequestCount = new LoginRequestCount();
                loginRequestCount.setLogin(login);
                loginRequestCount.setRequestCount(1L);
                loginRequestCountRepository.save(loginRequestCount);
            } catch (DataIntegrityViolationException e) {
                loginRequestCountRepository.increaseCounter(login);
            }
        }
    }

    @Autowired
    public void setLoginRequestCountRepository(LoginRequestCountRepository loginRequestCountRepository) {
        this.loginRequestCountRepository = loginRequestCountRepository;
    }
}
