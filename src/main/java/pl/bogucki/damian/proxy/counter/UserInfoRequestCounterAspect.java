package pl.bogucki.damian.proxy.counter;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.bogucki.damian.proxy.counter.service.UserInfoRequestCounterService;

@Aspect
@Component
public class UserInfoRequestCounterAspect {
    private static final Logger logger = LoggerFactory.getLogger(UserInfoRequestCounterAspect.class);
    private UserInfoRequestCounterService userInfoRequestCounterService;

    @Around("@annotation(UserInfoRequestCounter)")
    public Object count(ProceedingJoinPoint joinPoint) throws Throwable {
        Object login = joinPoint.getArgs()[0];
        logger.info("Increasing users counter, login: " + login);
        userInfoRequestCounterService.incrementCounter((String) login);
        return joinPoint.proceed();
    }

    @Autowired
    public void setUserInfoRequestCounterService(UserInfoRequestCounterService userInfoRequestCounterService) {
        this.userInfoRequestCounterService = userInfoRequestCounterService;
    }
}
