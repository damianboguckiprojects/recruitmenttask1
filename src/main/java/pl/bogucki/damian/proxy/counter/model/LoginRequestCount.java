package pl.bogucki.damian.proxy.counter.model;

import com.google.common.base.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "`LOGIN_REQUEST_COUNT`")
@Entity(name = "login_request_count")
public class LoginRequestCount {

    @Id
    @Column(name = "`ID`")
    @GeneratedValue
    private Long id;

    @Column(name = "`LOGIN`", unique = true, nullable = false)
    private String login;

    @Column(name = "`REQUEST_COUNT`", nullable = false)
    private Long requestCount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Long getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(Long requestCount) {
        this.requestCount = requestCount;
    }

    @Override
    public int hashCode(){
        return Objects.hashCode(id, login, requestCount);
    }

    @Override
    public boolean equals(Object that){
        if(!(that instanceof LoginRequestCount)){
            return false;
        }

        return Objects.equal(id,((LoginRequestCount) that).id)
                && Objects.equal(login, ((LoginRequestCount) that).login)
                && Objects.equal(requestCount, ((LoginRequestCount) that).requestCount);
    }
}
