package pl.bogucki.damian.proxy.counter.service;

public interface UserInfoRequestCounterService {
    void incrementCounter(String login);
}
