package pl.bogucki.damian.proxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserInfoProxyApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserInfoProxyApplication.class, args);
    }

}
