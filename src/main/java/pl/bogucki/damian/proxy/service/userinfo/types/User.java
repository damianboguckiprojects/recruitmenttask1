package pl.bogucki.damian.proxy.service.userinfo.types;

import pl.bogucki.damian.proxy.service.userinfo.exceptions.InvalidUserException;

public class User {
    private final String user;

    public User(String user) throws InvalidUserException {
        if(user == null || user.isEmpty()){
            throw new InvalidUserException("User is null or empty");
        }

        this.user = user;
    }

    public String value(){
        return this.user;
    }

    @Override
    public boolean equals(Object other){
        if(!(other instanceof User)){
            return false;
        }

        return this.user.equals(((User) other).user);
    }

    @Override
    public int hashCode(){
        return user.hashCode();
    }
}
