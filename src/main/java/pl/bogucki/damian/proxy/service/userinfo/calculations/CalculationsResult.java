package pl.bogucki.damian.proxy.service.userinfo.calculations;

public interface CalculationsResult {
    Double result();
}
