package pl.bogucki.damian.proxy.service.userinfo.calculations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.bogucki.damian.proxy.service.userinfo.calculations.types.Followers;
import pl.bogucki.damian.proxy.service.userinfo.calculations.types.PublicRepos;
import pl.bogucki.damian.proxy.service.userinfo.exceptions.CalculationsException;

public class DefaultCalculationsResult implements CalculationsResult {
    private final Logger logger = LoggerFactory.getLogger(DefaultCalculationsResult.class);

    private Double result;

    public DefaultCalculationsResult(Long followers, Long publicRepos) {
        logger.info("Performing calculations");

        try {
            Long followersVal = new Followers(followers).value();
            Long publicReposVal = new PublicRepos(publicRepos).value();

            if(followersVal.equals(0L)){
                throw new CalculationsException("Division by 0");
            }

            this.result = 6D/followersVal * (2D+ publicReposVal);

        }catch (CalculationsException e){
            logger.warn("Could not perform calculations", e);
            this.result = Double.NaN;
        }
    }


    @Override
    public Double result() {
        return this.result;
    }

    @Override
    public int hashCode(){
        return result.hashCode();
    }

    @Override
    public boolean equals(Object other){
        if(!(other instanceof DefaultCalculationsResult)){
            return false;
        }

        return this.result.equals(((DefaultCalculationsResult) other).result);
    }
}
