package pl.bogucki.damian.proxy.service.userinfo.calculations.types;

import pl.bogucki.damian.proxy.service.userinfo.exceptions.CalculationsException;

public class PublicRepos {
    private final Long publicRepos;

    public PublicRepos(Long publicRepos) throws CalculationsException {
        if (publicRepos == null || publicRepos < 0) {
            throw new CalculationsException("Invalid public_repos value: " + publicRepos);
        }

        this.publicRepos = publicRepos;
    }

    public Long value(){
        return this.publicRepos;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof PublicRepos)) {
            return false;
        }
        return this.publicRepos.equals(((PublicRepos) other).publicRepos);
    }

    @Override
    public int hashCode() {
        return publicRepos.hashCode();
    }
}
