package pl.bogucki.damian.proxy.service.userinfo.calculations.types;

import pl.bogucki.damian.proxy.service.userinfo.exceptions.CalculationsException;

public class Followers {
    private final Long followers;

    public Followers(Long followers) throws CalculationsException {
        if (followers == null || followers < 0L) {
            throw new CalculationsException("Invalid followers value: " + followers);
        }
        this.followers = followers;
    }

    public Long value() {
        return this.followers;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Followers)) {
            return false;
        }
        return this.followers.equals(((Followers) other).followers);
    }

    @Override
    public int hashCode() {
        return followers.hashCode();
    }
}
