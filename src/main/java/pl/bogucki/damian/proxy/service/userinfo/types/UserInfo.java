package pl.bogucki.damian.proxy.service.userinfo.types;

import com.google.common.base.Objects;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.joda.time.Instant;
import pl.bogucki.damian.proxy.service.userinfo.exceptions.InvalidGithubResponseException;

import java.net.MalformedURLException;
import java.net.URL;


public class UserInfo {

    private Long id;
    private String login;
    private String name;
    private String type;
    private URL avatarUrl;
    private Instant createdAt;
    private Double calculations;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAvatarUrl() {
        if(avatarUrl == null){
            return null;
        }
        return avatarUrl.toString();
    }

    public void setAvatarUrl(String avatarUrl) throws InvalidGithubResponseException {
        try {
            this.avatarUrl = new URL(avatarUrl);
        } catch (MalformedURLException e) {
            throw new InvalidGithubResponseException("Invalid avatar url", e);
        }
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public void setCreatedAt(String createdAt) throws InvalidGithubResponseException {
        if (createdAt != null) {
            try {
                this.createdAt = Instant.parse(createdAt);
            } catch (IllegalArgumentException e) {
                throw new InvalidGithubResponseException("Invalid date format", e);
            }
        }

    }

    public Double getCalculations() {
        return calculations;
    }

    public void setCalculations(Double calculations) {
        this.calculations = calculations;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, login, name, type, avatarUrl, createdAt, calculations);
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof UserInfo)) {
            return false;
        }

        UserInfo that = (UserInfo) other;

        return Objects.equal(this.id, that.id)
                && Objects.equal(this.login, that.login)
                && Objects.equal(this.name, that.name)
                && Objects.equal(this.type, that.type)
                && Objects.equal(this.avatarUrl, that.avatarUrl)
                && Objects.equal(this.createdAt, that.createdAt)
                && Objects.equal(this.calculations, that.calculations);
    }

}
