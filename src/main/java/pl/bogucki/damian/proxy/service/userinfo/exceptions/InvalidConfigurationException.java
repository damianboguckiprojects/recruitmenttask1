package pl.bogucki.damian.proxy.service.userinfo.exceptions;

public class InvalidConfigurationException extends Exception{
    public InvalidConfigurationException(String msg){
        super(msg);
    }
}
