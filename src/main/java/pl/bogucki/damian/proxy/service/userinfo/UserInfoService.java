package pl.bogucki.damian.proxy.service.userinfo;


import pl.bogucki.damian.proxy.service.userinfo.exceptions.InvalidGithubResponseException;
import pl.bogucki.damian.proxy.service.userinfo.types.User;
import pl.bogucki.damian.proxy.service.userinfo.types.UserInfo;

import java.net.MalformedURLException;
import java.net.URISyntaxException;

public interface UserInfoService {
    UserInfo createUserInfoAndCalculations(User user) throws InvalidGithubResponseException, MalformedURLException, URISyntaxException;
}
