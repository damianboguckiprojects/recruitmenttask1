package pl.bogucki.damian.proxy.service.userinfo.exceptions;

public class InvalidGithubResponseException extends Exception {
    public InvalidGithubResponseException(String msg) {
        super(msg);
    }

    public InvalidGithubResponseException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
