package pl.bogucki.damian.proxy.service.userinfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.bogucki.damian.proxy.service.userinfo.calculations.DefaultCalculationsResult;
import pl.bogucki.damian.proxy.service.userinfo.exceptions.InvalidConfigurationException;
import pl.bogucki.damian.proxy.service.userinfo.exceptions.InvalidGithubResponseException;
import pl.bogucki.damian.proxy.service.userinfo.types.GithubUserInfoResponse;
import pl.bogucki.damian.proxy.service.userinfo.types.User;
import pl.bogucki.damian.proxy.service.userinfo.types.UserInfo;

import java.net.URI;
import java.net.URISyntaxException;

@Component
public class UserInfoServiceImpl implements UserInfoService {
    private Logger logger = LoggerFactory.getLogger(UserInfoServiceImpl.class);

    private RestTemplate restTemplate;
    private String githubUserInfoUri;

    @Override
    public UserInfo createUserInfoAndCalculations(User user) throws InvalidGithubResponseException, URISyntaxException {
        GithubUserInfoResponse githubUserInfoResponse = callService(user);

        UserInfo userInfo = new UserInfo();
        userInfo.setId(githubUserInfoResponse.getId());
        userInfo.setLogin(githubUserInfoResponse.getLogin());
        userInfo.setName(githubUserInfoResponse.getName());
        userInfo.setType(githubUserInfoResponse.getType());
        userInfo.setAvatarUrl(githubUserInfoResponse.getAvatarUrl());
        userInfo.setCreatedAt(githubUserInfoResponse.getCreatedAt());
        userInfo.setCalculations(
                new DefaultCalculationsResult(githubUserInfoResponse.getFollowers(), githubUserInfoResponse.getPublicRepos())
                        .result());

        return userInfo;
    }


    private GithubUserInfoResponse callService(User user) throws InvalidGithubResponseException, URISyntaxException {
        URI addr = new URI(githubUserInfoUri + user.value());
        logger.info("Calling github users api: "+addr.toString());
        ResponseEntity<GithubUserInfoResponse> resp = restTemplate.getForEntity(addr, GithubUserInfoResponse.class);

        if (!HttpStatus.OK.equals(resp.getStatusCode())) {
            throw new InvalidGithubResponseException("Invalid github user info response status: " + resp.getStatusCode());
        }

        if (resp.getBody() == null) {
            throw new InvalidGithubResponseException("Invalid github user info response.");
        }

        return resp.getBody();
    }

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Value("${app.githubUserInfoUri}")
    public void setGithubUserInfoUri(String githubUserInfoUri) throws InvalidConfigurationException {
        if(!githubUserInfoUri.endsWith("/")){
            throw new InvalidConfigurationException("Invalid githubUserInfoUri. Uri should end /");
        }

        this.githubUserInfoUri = githubUserInfoUri;
    }
}
