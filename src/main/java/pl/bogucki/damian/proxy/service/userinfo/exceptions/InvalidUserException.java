package pl.bogucki.damian.proxy.service.userinfo.exceptions;

public class InvalidUserException extends Exception{
    public InvalidUserException(String msg){
        super(msg);
    }
}
