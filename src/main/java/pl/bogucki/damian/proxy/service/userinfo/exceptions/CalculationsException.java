package pl.bogucki.damian.proxy.service.userinfo.exceptions;

public class CalculationsException extends Exception {
    public CalculationsException(String msg) {
        super(msg);
    }

    public CalculationsException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
