package pl.bogucki.damian.proxy.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import pl.bogucki.damian.proxy.controller.model.UserResponse;
import pl.bogucki.damian.proxy.converter.UserInfoToUserResponseConverter;
import pl.bogucki.damian.proxy.counter.UserInfoRequestCounter;
import pl.bogucki.damian.proxy.service.userinfo.UserInfoService;
import pl.bogucki.damian.proxy.service.userinfo.exceptions.InvalidGithubResponseException;
import pl.bogucki.damian.proxy.service.userinfo.exceptions.InvalidUserException;
import pl.bogucki.damian.proxy.service.userinfo.types.User;
import pl.bogucki.damian.proxy.service.userinfo.types.UserInfo;

import java.net.MalformedURLException;
import java.net.URISyntaxException;

@RestController
public class UsersController {

    private final Logger logger = LoggerFactory.getLogger(UsersController.class);

    private UserInfoService usersService;

    //match:
    //no start hype
    //no double hypes
    //not longer than 39 (any letter and any number)
    //no end hype
    //login must be first argument
    @UserInfoRequestCounter
    @GetMapping("users/{login:?:^(?!-)(?!.*--)([A-Za-z0-9\\-]{0,39})(?<!-)$}")
    public ResponseEntity<UserResponse> getUsers(@PathVariable("login") String login) throws InvalidGithubResponseException, MalformedURLException, InvalidUserException, URISyntaxException {
        logger.info("Got request login: " + login);

        UserInfo userInfo = usersService.createUserInfoAndCalculations(new User(login));

        UserResponse userResponse = UserInfoToUserResponseConverter.convert(userInfo);

        logger.info("Processing succeed. Returning status code: 200");
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    @ExceptionHandler(HttpClientErrorException.NotFound.class)
    public ResponseEntity<Void> handleNotFound(Exception e) {
        logger.error("Returning status code: 404", e);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({InvalidGithubResponseException.class, InvalidUserException.class})
    public ResponseEntity<Void> handleException(Exception e) {
        logger.error("Returning status code: 422");
        return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @Autowired
    public void setUsersService(UserInfoService usersService) {
        this.usersService = usersService;
    }
}
