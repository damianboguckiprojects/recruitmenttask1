package pl.bogucki.damian.proxy.controller.model;

import com.google.common.base.Objects;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.joda.time.Instant;

public class UserResponse {
    private Long id;
    private String login;
    private String name;
    private String type;
    private String avatarUrl;
    private Instant createdAt;
    private Double calculations;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Double getCalculations() {
        return calculations;
    }

    public void setCalculations(Double calculations) {
        this.calculations = calculations;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof UserResponse)) {
            return false;
        }

        UserResponse that = (UserResponse) other;

        return Objects.equal(id, that.id)
                && Objects.equal(login, that.login)
                && Objects.equal(type, that.type)
                && Objects.equal(avatarUrl, that.avatarUrl)
                && Objects.equal(createdAt, that.createdAt)
                && Objects.equal(calculations, that.calculations);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, login, name, type, avatarUrl, createdAt, calculations);
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
