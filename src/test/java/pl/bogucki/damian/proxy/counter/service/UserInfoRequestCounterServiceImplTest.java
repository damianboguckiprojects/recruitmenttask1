package pl.bogucki.damian.proxy.counter.service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;
import pl.bogucki.damian.proxy.counter.model.LoginRequestCount;
import pl.bogucki.damian.proxy.counter.repository.LoginRequestCountRepository;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class UserInfoRequestCounterServiceImplTest {
    @Mock
    private static LoginRequestCountRepository loginRequestCountRepository;

    private static LoginRequestCount loginRequestCount;

    @BeforeAll
    public static void init(){
        loginRequestCount = new LoginRequestCount();
        loginRequestCount.setLogin("test");
        loginRequestCount.setRequestCount(1L);
    }

    @Test
    public void verifyWhenEntryNotExist(){
        when(loginRequestCountRepository.increaseCounter("test"))
                .thenReturn(0);

        when(loginRequestCountRepository.save(loginRequestCount))
                .thenReturn(loginRequestCount);

        UserInfoRequestCounterServiceImpl counterService = new UserInfoRequestCounterServiceImpl();
        counterService.setLoginRequestCountRepository(loginRequestCountRepository);

        counterService.incrementCounter("test");

        verify(loginRequestCountRepository, times(1)).increaseCounter("test");
        verify(loginRequestCountRepository, times(1)).save(loginRequestCount);

    }

    @Test
    public void verifyWhenEntryCreatedDuringExecutionSave(){
        when(loginRequestCountRepository.increaseCounter("test"))
                .thenReturn(0);

        when(loginRequestCountRepository.save(loginRequestCount))
                .thenThrow(DataIntegrityViolationException.class);

        UserInfoRequestCounterServiceImpl counterService = new UserInfoRequestCounterServiceImpl();
        counterService.setLoginRequestCountRepository(loginRequestCountRepository);

        counterService.incrementCounter("test");

        verify(loginRequestCountRepository, times(2)).increaseCounter("test");
        verify(loginRequestCountRepository, times(1)).save(loginRequestCount);

    }

    @Test
    public void verifyWhenEntryExist(){
        when(loginRequestCountRepository.increaseCounter("test"))
                .thenReturn(1);

        UserInfoRequestCounterServiceImpl counterService = new UserInfoRequestCounterServiceImpl();
        counterService.setLoginRequestCountRepository(loginRequestCountRepository);

        counterService.incrementCounter("test");

        verify(loginRequestCountRepository, times(1)).increaseCounter("test");
        verify(loginRequestCountRepository, times(0)).save(loginRequestCount);

    }
}