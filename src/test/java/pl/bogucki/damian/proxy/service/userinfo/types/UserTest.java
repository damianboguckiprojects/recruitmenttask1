package pl.bogucki.damian.proxy.service.userinfo.types;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import pl.bogucki.damian.proxy.service.userinfo.exceptions.CalculationsException;
import pl.bogucki.damian.proxy.service.userinfo.exceptions.InvalidUserException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UserTest {
    @Test
    public void shouldThrowInvalidUserExceptionWhenEmptyLogin(){
        assertThrows(InvalidUserException.class,
                ()-> new User(""));
    }

    @Test
    public void shouldThrowInvalidUserExceptionWhenNullLogin(){
        assertThrows(InvalidUserException.class,
                ()-> new User(null));
    }

    @ParameterizedTest
    @ValueSource(strings = {"asd","test","asd-asdda","132-312-2"})
    public void shouldInitializeWithCorrectValue(String login) throws CalculationsException, InvalidUserException {
        assertEquals(login, new User(login).value());
    }

}