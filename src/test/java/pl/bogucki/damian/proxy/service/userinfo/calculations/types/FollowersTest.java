package pl.bogucki.damian.proxy.service.userinfo.calculations.types;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import pl.bogucki.damian.proxy.service.userinfo.exceptions.CalculationsException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FollowersTest {
    @Test
    public void shouldThrowCalculationsExceptionCuzOfNullValue() {
        assertThrows(
                CalculationsException.class,
                () -> new Followers(null));
    }

    @Test
    public void shouldThrowCalculationsExceptionCuzOfNegativeValue() {
        assertThrows(
                CalculationsException.class,
                () -> new Followers(-14L));
    }

    @ParameterizedTest
    @ValueSource(longs = {0L, 1L, 3L, 5L, 5555L, 15L, Long.MAX_VALUE})
    public void shouldInitializeWithCorrectValueForNotNegativeLongs(Long number) throws CalculationsException {
        assertEquals(number, new Followers(number).value());
    }

    @Test
    public void hashCodeAndEqualsTestEquals() throws CalculationsException {
        Followers followers1 = new Followers(40L);
        Followers followers2 = new Followers(40L);

        assertEquals(followers2, followers1);
        assertEquals(followers1.hashCode(), followers2.hashCode());
    }

    @Test
    public void equalsTestNotEquals() throws CalculationsException {
        Followers followers1 = new Followers(11L);
        Followers followers2 = new Followers(40L);

        assertNotEquals(followers2, followers1);
    }
}