package pl.bogucki.damian.proxy.service.userinfo.types;

import org.junit.jupiter.api.Test;
import pl.bogucki.damian.proxy.service.userinfo.exceptions.InvalidGithubResponseException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserInfoTest {

    @Test
    public void shouldThrowGithubUserInfoResponseExceptionCuzOfUrl() throws InvalidGithubResponseException {
        assertThrows(InvalidGithubResponseException.class, ()->new UserInfo().setAvatarUrl("httas://localhost:8080/test"));
    }

    @Test
    public void shouldThrowGithubUserInfoResponseExceptionCuzOfDate() throws InvalidGithubResponseException {
        assertThrows(InvalidGithubResponseException.class, ()->new UserInfo().setCreatedAt("2021-08-23T1428:41Z"));
    }

    @Test
    public void hashCodeAndEqualsTest() throws InvalidGithubResponseException {
        UserInfo userInfo = new UserInfo();
        UserInfo userInfo1 = new UserInfo();

        userInfo.setType("TEST_TYPE");
        userInfo1.setType("TEST_TYPE");
        userInfo.setId(50L);
        userInfo1.setId(50L);
        userInfo.setAvatarUrl("http://localhost:8080/test");
        userInfo1.setAvatarUrl("http://localhost:8080/test");

        assertTrue(userInfo.equals(userInfo1));
        assertEquals(userInfo.hashCode(), userInfo1.hashCode());
    }
}