package pl.bogucki.damian.proxy.service.userinfo.calculations.types;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import pl.bogucki.damian.proxy.service.userinfo.exceptions.CalculationsException;

import static org.junit.jupiter.api.Assertions.*;

class PublicReposTest {
    @Test
    public void shouldThrowCalculationsExceptionCuzOfNullValue() {
        assertThrows(
                CalculationsException.class,
                () -> new PublicRepos(null));
    }

    @Test
    public void shouldThrowCalculationsExceptionCuzOfNegativeValue() {
        assertThrows(
                CalculationsException.class,
                () -> new PublicRepos(-14L));
    }

    @ParameterizedTest
    @ValueSource(longs = {0L, 1L, 3L, 5L, 5555L, 15L, Long.MAX_VALUE})
    public void shouldInitializeWithCorrectValueForNotNegativeLongs(Long number) throws CalculationsException {
        assertEquals(number, new PublicRepos(number).value());
    }

    @Test
    public void hashCodeAndEqualsTest() throws CalculationsException {
        PublicRepos publicRepos1 = new PublicRepos(40L);
        PublicRepos publicRepos2 = new PublicRepos(40L);

        assertEquals(publicRepos2, publicRepos1);
        assertEquals(publicRepos1.hashCode(), publicRepos2.hashCode());
    }

    @Test
    public void equalsTestNotEquals() throws CalculationsException {
        PublicRepos publicRepos1 = new PublicRepos(40L);
        PublicRepos publicRepos2 = new PublicRepos(15L);

        assertNotEquals(publicRepos1, publicRepos2);
    }
}