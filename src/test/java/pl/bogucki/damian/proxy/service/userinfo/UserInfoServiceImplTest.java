package pl.bogucki.damian.proxy.service.userinfo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import org.joda.time.Instant;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import pl.bogucki.damian.proxy.service.userinfo.exceptions.InvalidConfigurationException;
import pl.bogucki.damian.proxy.service.userinfo.exceptions.InvalidGithubResponseException;
import pl.bogucki.damian.proxy.service.userinfo.exceptions.InvalidUserException;
import pl.bogucki.damian.proxy.service.userinfo.types.GithubUserInfoResponse;
import pl.bogucki.damian.proxy.service.userinfo.types.User;
import pl.bogucki.damian.proxy.service.userinfo.types.UserInfo;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserInfoServiceImplTest {

    @Mock
    private static RestTemplate restTemplate;

    private static GithubUserInfoResponse githubUserInfoResponse;
    private static ResponseEntity<GithubUserInfoResponse> fineResponse;
    private static URI uri;
    private static User user;

    @BeforeAll
    public static void loadJson() throws IOException, URISyntaxException, InvalidUserException {
        user = new User("test");
        uri = new URI("http://localhost:8081/test/test");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JodaModule());
        githubUserInfoResponse = objectMapper.readValue(
                ClassLoader.getSystemResourceAsStream("schema/githubUserInfoResponse.json"),
                GithubUserInfoResponse.class);

        fineResponse = new ResponseEntity<>(githubUserInfoResponse, HttpStatus.OK);
    }

    @Test
    public void shouldThrowInvalidGithubResponseBadStatusCode() throws InvalidConfigurationException {
        when(restTemplate.getForEntity(uri, GithubUserInfoResponse.class))
                .thenReturn(new ResponseEntity<>(HttpStatus.MOVED_PERMANENTLY));

        UserInfoServiceImpl userInfoService = new UserInfoServiceImpl();
        userInfoService.setRestTemplate(restTemplate);
        userInfoService.setGithubUserInfoUri("http://localhost:8081/test/");

        assertThrows(InvalidGithubResponseException.class,
                ()-> userInfoService.createUserInfoAndCalculations(user));
    }

    @Test
    public void shouldThrowInvalidGithubResponseNoResponseBody() throws InvalidConfigurationException {
        when(restTemplate.getForEntity(uri, GithubUserInfoResponse.class))
                .thenReturn(new ResponseEntity<>(HttpStatus.OK));

        UserInfoServiceImpl userInfoService = new UserInfoServiceImpl();
        userInfoService.setRestTemplate(restTemplate);
        userInfoService.setGithubUserInfoUri("http://localhost:8081/test/");

        assertThrows(InvalidGithubResponseException.class,
                ()-> userInfoService.createUserInfoAndCalculations(user));
    }

    @Test
    public void createUserInfoAndCalculationsTest() throws InvalidUserException, InvalidGithubResponseException, MalformedURLException, URISyntaxException, InvalidConfigurationException {


        when(restTemplate.getForEntity(uri, GithubUserInfoResponse.class))
                .thenReturn(fineResponse);

        UserInfoServiceImpl userInfoService = new UserInfoServiceImpl();
        userInfoService.setRestTemplate(restTemplate);
        userInfoService.setGithubUserInfoUri("http://localhost:8081/test/");

        UserInfo userInfo = userInfoService.createUserInfoAndCalculations(user);

        assertEquals(githubUserInfoResponse.getId(), userInfo.getId());
        assertEquals(githubUserInfoResponse.getLogin(), userInfo.getLogin());
        assertEquals(githubUserInfoResponse.getName(), userInfo.getName());
        assertEquals(githubUserInfoResponse.getAvatarUrl(), userInfo.getAvatarUrl());
        assertEquals(githubUserInfoResponse.getType(), userInfo.getType());
        assertEquals(Instant.parse(githubUserInfoResponse.getCreatedAt()), userInfo.getCreatedAt());

        Double result = 6D / githubUserInfoResponse.getFollowers() * (2D + githubUserInfoResponse.getPublicRepos());

        assertEquals(result, userInfo.getCalculations());

    }

}