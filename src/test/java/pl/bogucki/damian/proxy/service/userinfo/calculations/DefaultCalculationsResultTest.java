package pl.bogucki.damian.proxy.service.userinfo.calculations;

import org.junit.jupiter.api.Test;
import pl.bogucki.damian.proxy.service.userinfo.exceptions.CalculationsException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DefaultCalculationsResultTest {

    @Test
    public void shouldReturnNaNWhenFollowers0CuzOfDivisionBy0(){
        assertEquals(Double.NaN, new DefaultCalculationsResult(0L,0L).result());
    }

    @Test
    public void shouldReturnNaNWhenAnyOfParamsIsNull(){
        assertEquals(Double.NaN, new DefaultCalculationsResult(0L,null).result());
        assertEquals(Double.NaN, new DefaultCalculationsResult(null,0L).result());
        assertEquals(Double.NaN, new DefaultCalculationsResult(null,null).result());
    }

    @Test
    public void shouldCalculateFine(){
        assertEquals(1.2011999999999998D, new DefaultCalculationsResult(5000L, 999L).result());
        assertEquals(0.14832D, new DefaultCalculationsResult(50000L, 1234L).result());
        assertEquals(0.8D, new DefaultCalculationsResult(15L, 0L).result());
    }

    @Test
    public void hashCodeAndEqualsTest() throws CalculationsException {
        DefaultCalculationsResult defaultCalculationsResult1 = new DefaultCalculationsResult(50L,50L);
        DefaultCalculationsResult defaultCalculationsResult2 = new DefaultCalculationsResult(50L,50L);

        assertTrue(defaultCalculationsResult1.equals(defaultCalculationsResult2));
        assertEquals(defaultCalculationsResult1.hashCode(), defaultCalculationsResult1.hashCode());
    }

    @Test
    public void equalsTestNotEquals() throws CalculationsException {
        DefaultCalculationsResult defaultCalculationsResult1 = new DefaultCalculationsResult(50L,50L);
        DefaultCalculationsResult defaultCalculationsResult2 = new DefaultCalculationsResult(50L,51L);

        assertNotEquals(defaultCalculationsResult1, defaultCalculationsResult2);
    }

}