package pl.bogucki.damian.proxy.controller;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import pl.bogucki.damian.proxy.service.userinfo.UserInfoService;
import pl.bogucki.damian.proxy.service.userinfo.types.User;
import pl.bogucki.damian.proxy.service.userinfo.types.UserInfo;

import java.net.URI;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = UsersController.class)
class UsersControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserInfoService userInfoService;

    private User user;

    @ParameterizedTest
    @ValueSource(strings = {"test", "assad-d", "octocat", "cat-cat-cat"})
    public void testCorrectUrl(String login) throws Exception {
        UserInfo userInfo = new UserInfo();
        userInfo.setLogin(login);

        when(userInfoService.createUserInfoAndCalculations(new User(login)))
                .thenReturn(userInfo);

        mockMvc.perform(get(new URI("/users/" + login)))
                .andExpect(status().isOk());

    }

    @ParameterizedTest
    @ValueSource(strings = {"test-", "assad--d", "_octocat422", "cat-cat/-cat"})
    public void testInvalidUrl(String login) throws Exception {
        UserInfo userInfo = new UserInfo();
        userInfo.setLogin(login);

        when(userInfoService.createUserInfoAndCalculations(new User(login)))
                .thenReturn(userInfo);

        mockMvc.perform(get(new URI("/users/" + login)))
                .andExpect(status().isNotFound());

    }

}