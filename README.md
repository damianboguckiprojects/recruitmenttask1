Proxy to call github api and perform some calculations with request counter

API documentation:
property
springfox.documentation.enabled=true
enables swagger-ui at:
http://localhost:8080/swagger-ui/

BUILD:
mvnw clean install

Start app and database in docker:
docker-compose up

Required configuration properties (example values):
spring.datasource.url=jdbc:postgresql://localhost:5432/test
spring.datasource.username=postgres
spring.datasource.password=postgres
app.githubUserInfoUri = https://api.github.com/users/

app.githubUserInfoUri has to end with '/'


